<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Auth-Token');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('/register', 'Auth\RegisterController@register');

Route::post('login', 'API\PassportController@login');
Route::post('register', 'API\PassportController@register');
Route::post('forgot','API\PassportController@forgot');

Route::group(['middleware' => 'auth:api'], function(){
	// Route::post('details', 'API\PassportController@getDetails');

	Route::prefix('user')->group(function(){
		Route::get('list','API\UserController@read');
		Route::get('notifications','API\UserController@notifications');
		Route::get('detail/{id}','API\UserController@readDetail');
		Route::put('detail','API\UserController@update');
		Route::put('profile','API\UserController@profile');
	});

	Route::prefix('post')->group(function(){
		Route::get('list','API\PostController@read');
		Route::get('detail/{id}','API\PostController@readDetail');
		Route::post('detail','API\PostController@create');
		Route::post('comment','API\PostController@comment');
		Route::post('like','API\PostController@like');		
		Route::put('detail','API\PostController@update');
		Route::delete('detail/{id}','API\PostController@delete');
	});

	Route::prefix('task')->group(function(){
		Route::get('list','API\TasksController@read');
		Route::get('detail/{id}','API\TasksController@readDetail');
		Route::post('detail','API\TasksController@create');
		Route::put('detail','API\TasksController@update');
		Route::delete('detail/{id}','API\TasksController@delete');
	});

	Route::prefix('faq')->group(function(){
		Route::get('list','API\FaqController@read');
		Route::get('detail/{id}','API\FaqController@readDetail');
		Route::post('detail','API\FaqController@create');
		Route::put('detail','API\FaqController@update');
		Route::delete('detail/{id}','API\FaqController@delete');
	});
});
