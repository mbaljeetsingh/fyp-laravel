<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;

class question extends Model
{
	// $lic = Role::create(['name' => 'liCoordinator']);
	// $pi = Role::create(['name' => 'preIntern']);
	// $is = Role::create(['name' => 'indusStudent']);
	// $permission_r = Permission::create(['name' => 'read qa']);
	// $permission_c = Permission::create(['name' => 'create qa']);
	// $permission_e = Permission::create(['name' => 'edit qa']);
	// $permission_d = Permission::create(['name' => 'delete qa']);

	protected $fillable = [
        'question', 'answer'
    ];
}
