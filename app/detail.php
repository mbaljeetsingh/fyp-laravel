<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class detail extends Model
{
    $lic = Role::create(['name' => 'liCoordinator']);
    $pi = Role::create(['name' => 'preIntern']);
    $is = Role::create(['name' => 'indusStudent']);
    $permission = Permission::create(['name' => 'read detail']);
	$permission = Permission::create(['name' => 'create detail']);
	$permission = Permission::create(['name' => 'edit detail']);
	$permission = Permission::create(['name' => 'delete detail']);
}
