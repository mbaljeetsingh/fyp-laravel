<?php

namespace App\Http\Controllers\API;

use App\post;
use App\User;
use App\Comment;
use App\Like;
use App\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Validator;

class PostController extends Controller
{
	public $successStatus = 200;

	public function read(){
		$post = post::with('user', 'likes', 'likes.user', 'comments', 'comments.user')->get();
		return response()->json(['success' => $post], $this->successStatus);
	}

	public function readDetail($id){
		$post = post::find($id);
		return response()->json(['success' => $post], $this->successStatus);
	}

	// put x-www-form-urlencoded
	public function update(Request $req){

		$data = $req->json()->all();

		$post = post::find($data['id']);

		$post->title = $data['title'] != '' ? $data['title'] : $post->title;
		$post->content = $data['content'] != '' ? $data['content'] : $post->content;
		$post->role = $data['role'] != '' ? $data['role'] : $post->role;

		$result = $post->save();
		$post['user'] = User::where('id', $req->json()->all()['user_id'])->first();

		return response()->json(['success' => $post], $this->successStatus);
	}

	public function delete($id){
		$post = post::destroy($id);
		return response()->json(['success' => $id], $this->successStatus);
	}

	// post x-www-form-urlencoded
	public function create(Request $req){
		$validator = Validator::make($req->json()->all(), [
            'content' => 'required',
        ]);

		if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$post = post::create($req->json()->all());
		$post['user'] = User::where('id', $req->json()->all()['user_id'])->first();

		$data = $req->json()->all();

		Notification::create([
			'role' => $data['role'],
			'content' => '[Post] '.$data['title']
		]);

		return response()->json(['success' => $post], $this->successStatus);
	}

	// post x-www-form-urlencoded
	public function comment(Request $req){
		$validator = Validator::make($req->json()->all(), [
            'content' => 'required',
        ]);

		if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$comment = Comment::create($req->json()->all());
		$comment['user'] = User::where('id', $req->json()->all()['user_id'])->first();
		return response()->json(['success' => $comment], $this->successStatus);
	}

	public function like(Request $req){
		$like = Like::create($req->json()->all());
		$like['user'] = User::where('id', $req->json()->all()['user_id'])->first();
		return response()->json(['success' => $like], $this->successStatus);
	}
}
