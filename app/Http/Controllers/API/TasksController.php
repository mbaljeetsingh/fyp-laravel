<?php

namespace App\Http\Controllers\API;

use App\task;
use App\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Validator;

class TasksController extends Controller
{
	public $successStatus = 200;

	public function read(){
		$task = task::all();
		return response()->json(['success' => $task], $this->successStatus);
	}

	public function readDetail($id){
		$task = task::find($id);
		return response()->json(['success' => $task], $this->successStatus);
	}

	// put x-www-form-urlencoded
	public function update(Request $req){

		$data = $req->json()->all();

		$ts = task::find($data['id']);

		$ts->title = $data['title'] != '' ? $data['title'] : $ts->title;
		// $ts->body = $data['body'] != '' ? $data['body'] : $ts->body;
		$ts->role = $data['role'];

		$result = $ts->save();

		return response()->json(['success' => $result], $this->successStatus);
	}

	public function delete($id){
		$td = task::destroy($id);
		return response()->json(['success' => $td], $this->successStatus);
	}

	// post x-www-form-urlencoded
	public function create(Request $req){
		$validator = Validator::make($req->json()->all(), [
            'title' => 'required',
            // 'body' => 'required',
        ]);

		if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

		$task = task::create($req->json()->all());

		$data = $req->json()->all();

		Notification::create([
			'role' => $data['role'],
			'content' => '[Announcment] '.$data['title']
		]);

		return response()->json(['success' => $task], $this->successStatus);
	}
}
