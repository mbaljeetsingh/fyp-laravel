<?php

namespace App\Http\Controllers\API;

use App\question;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class FaqController extends Controller
{
    public $successStatus = 200;
    
	public function read(){
		$faq = question::all();
		return response()->json(['success' => $faq], $this->successStatus);
	}

	public function readDetail($id){
		$faq = question::find($id);
		return response()->json(['success' => $faq], $this->successStatus);
	}

	// put x-www-form-urlencoded
	public function update(Request $req){

		$data = $req->json()->all();

		$faq = question::find($data['id']);

		$faq->question = $data['question'] != '' ? $data['question'] : $faq->question;
		$faq->answer = $data['answer'] != '' ? $data['answer'] : $faq->answer;

		$result = $faq->save();

		return response()->json(['success' => $result], $this->successStatus);
	}

	public function delete($id){
		$td = question::destroy($id);
		return response()->json(['success' => $td], $this->successStatus);
	}

	// post x-www-form-urlencoded
	public function create(Request $req){
		$validator = Validator::make($req->json()->all(), [
            'question' => 'required',
            'answer' => 'required',
        ]);

		if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

		$task = question::create($req->json()->all());
		return response()->json(['success' => $task], $this->successStatus);
	}
}
