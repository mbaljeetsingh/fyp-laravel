<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class UserController extends Controller
{
	public $successStatus = 200;

	public function read(){
		$user = user::all();
		return response()->json(['success' => $user], $this->successStatus);
	}

	public function notifications(){
		$notifications = Notification::all();
		return response()->json(['success' => $notifications], $this->successStatus);
	}

	public function readDetail($id){
		$user = user::find($id);
		return response()->json(['success' => $user], $this->successStatus);
	}

	// put x-www-form-urlencoded
	public function update(Request $req){

		$data = $req->json()->all();

		$user = user::find($data['id']);

		$user->email = $data['email'] != '' ? $data['email'] : $user->email;
		$user->password = bcrypt($data['password']) != '' ? bcrypt($data['password']) : $user->password;

		$result = $user->save();

		return response()->json(['success' => $result], $this->successStatus);
	}

	public function profile(Request $req){

		$data = $req->json()->all();

		$user = user::find($data['id']);

		$user->name = $data['name'] != '' ? $data['name'] : $user->name;
		$user->email = $data['email'] != '' ? $data['email'] : $user->email;
		$user->role = $data['role'] != '' ? $data['role'] : $user->role;

		$result = $user->save();

		return response()->json(['success' => $result], $this->successStatus);
	}
}
