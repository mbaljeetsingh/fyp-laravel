<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class PassportController extends Controller
{
    public $successStatus = 200;

    public function login(Request $req){

        $form = $req->json()->all();

        // if(Auth::attempt(['email' => $form['email'], 'password' => $form['password']])){

        if(Auth::attempt(['student_id' => $form['email'], 'password' => $form['password']])){
            $user = Auth::user();
            $success['user-access-token'] =  $user->createToken('libapp')->accessToken;
            $success['code'] = $this->successStatus;
            $success['userData'] = $user;
            // user::with('role')->where('id', 19)->first()
            return response()->json(['data' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
            // return response()->json(['error'=>$form['email']], 401);
        }
    }

    public function forgot(Request $req){
        $data = $req->json()->all();

        $user = user::where('name', $data['name'])
                    ->where('student_id', $data['student_id'])
                    ->first();

		$user->password = bcrypt($data['password']) != '' ? bcrypt($data['password']) : $user->password;

        $result = $user->save();

		return response()->json(['success' => $result], $this->successStatus);
    }


    /*
     * php artisan passport:client --personal ---> fix accesstoken error ****
     */
    public function register(Request $request)
    {
        $data = $request->json()->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->json()->all();
        $input['password'] = bcrypt($input['password']);
        $input['role'] = 'normal';
        $input['status'] = 1;
        $user = User::create($input);

        $success['token'] =  $user->createToken('libapp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json($success, $this->successStatus);
    }

    public function getDetails()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
