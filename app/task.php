<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class task extends Model
{
	// role initiation
 //    $lic = Role::create(['name' => 'liCoordinator']);
 //    $pi = Role::create(['name' => 'preIntern']);
 //    $is = Role::create(['name' => 'indusStudent']);
 //    $permission_r = Permission::create(['name' => 'read task']);
	// $permission_c = Permission::create(['name' => 'create task']);
	// $permission_e = Permission::create(['name' => 'edit task']);
	// $permission_d = Permission::create(['name' => 'delete task']);

	// // role assign
	// $lic->givePermissionTo($permission_r,$permission_c,$permission_e,$permission_d);
	// $pi->givePermissionTo($permission_r);
	// $is->givePermissionTo($permission_r);
	protected $fillable = [
        'title', 'body', 'role'
    ];
}
