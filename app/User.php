<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * generate token for registered user
     */
    // public function generateToken()
    // {
    //     $this->remember_token = str_random(60);
    //     $this->save();

    //     return $this->remember_token;
    // }

    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }

    // public function role() {
    //     return $this->hasOne('App\Role', 'id', 'role');
    // }
}
