<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'role', 'content'
    ];

    public $timestamps = false;
}
