<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class post extends Model
{
  protected $fillable = [
        'title', 'content', 'user_id', 'role'
    ];
    protected $appends = ['total_likes', 'total_comments'];


    public function getTotalLikesAttribute()
    {
        return count($this->likes);
    }

    public function getTotalCommentsAttribute()
    {
        return count($this->comments);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
